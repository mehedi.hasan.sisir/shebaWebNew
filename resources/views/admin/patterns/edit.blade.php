@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Update Pattern</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Update A Pattern </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('patterns.update', $pattern->_id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')

                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input name="name"  value="{{ $pattern->name }}" required type="text" class="form-control" placeholder="Please Enter Ride Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Ride Type</label>
                                <div class="col-lg-8">
                                    <select name="ride_type_id" required class="form-control">
                                        @foreach($rideTypes as $rideType)

                                            <option @if($pattern->ride_type_id == $rideType->_id) selected='selected' @endif value="{{$rideType->_id}}">{{$rideType->name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Range</label>
                                <div class="col-lg-8">
                                    <input name="range"  value="{{ $pattern->range }}" required type="number" step="0.01" class="form-control" placeholder="Please Enter Range">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Base Price</label>
                                <div class="col-lg-8">
                                    <input name="base_price" value="{{ $pattern->base_price }}" required type="number" step="0.01"  class="form-control" placeholder="Please Enter Base Price">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Minimum Ride Fare</label>
                                <div class="col-lg-8">
                                    <input name="min_ride_fare" value="{{ $pattern->min_ride_fare }}" required type="number" step="0.01" class="form-control" placeholder="Please Enter Minimum Ride Fare">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Kilometer Rate</label>
                                <div class="col-lg-8">
                                    <input name="km_rate" value="{{ $pattern->km_rate }}" required type="number" step="0.01" class="form-control" placeholder="Please Enter Kilometer Rate">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Minimum Rate</label>
                                <div class="col-lg-8">
                                    <input name="min_rate" value="{{ $pattern->min_rate }}" required type="number" step="0.01" class="form-control" placeholder="Please Enter Minimum Rate">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Location</label>
                                <div class="col-lg-8">
                                    <input name="location" id="location" value="{{ $pattern->location_string }}" required type="text"
                                           class="form-control" placeholder="Please Enter Location">
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-lg-4 col-md-offset-4">
                                            <input name="latitude" value="{{ $pattern->latitude }}" readonly required type="text"
                                                   class="form-control" placeholder="Latitude">
                                        </div>
                                        <div class="col-lg-4">
                                            <input name="longitude" value="{{ $pattern->longitude }}" readonly required type="text"
                                                   class="form-control" placeholder="Longitude">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Currency <small class="text-danger">*auto added</small></label>
                                <div class="col-lg-8">
                                    <input readonly value="{{ $pattern->currency }}"  name="currency" type="text"
                                           class="form-control" placeholder="Please Enter Currency">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Status</label>
                                <div class="col-lg-8">
                                    <select name="status" required class="form-control">
                                        <option @if($pattern->status == 1) selected='selected' @endif  value="1">Active</option>
                                        <option @if($pattern->status == 0) selected='selected' @endif  value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update Pattern</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDZWL3BZwZLAwWcng7nGYKCBvyqMuV4mPA&libraries=places" type="text/javascript"></script>

    <script>

        var config_url = "{{url('/')}}";
        var autocomplete = new google.maps.places.Autocomplete((document.getElementById('location')), {types: ['geocode']});

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            var label_address  = place.adr_address;

            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $('input[name="latitude"]').val(lat);
            $('input[name="longitude"]').val(lng);
            var latlng = new google.maps.LatLng(lat, lng);

            new google.maps.Geocoder().geocode({'latLng' : latlng}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var country = null, countryCode = null, city = null, cityAlt = null;
                        var c, lc, component;
                        for (var r = 0, rl = results.length; r < rl; r += 1) {
                            var result = results[r];
                            if (!country && result.types[0] === 'country') {
                                country = result.address_components[0].long_name;
                                countryCode = result.address_components[0].short_name;
                            }

                            if (country) {
                                break;
                            }
                        }
                    }
                }
                var url = "{{url('currency-by-country')}}/"+country;
                $.get( url, function( data ) {

                    $('input[name="currency"]').val(data);
                });
            });
        });
    </script>
@endsection