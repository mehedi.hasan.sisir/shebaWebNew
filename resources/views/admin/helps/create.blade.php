@extends('layouts.app')

@section('style')

    <style>
        .wysihtml5-sandbox{

            width: 100%!important;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.min.css') }}" type="text/css"/>
@endsection

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Create Help Text</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Create New Help Text</h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('helps.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Head</label>
                                <div class="col-lg-10">
                                    <input name="head"  value="{{ old('head') }}" required type="text" class="form-control" placeholder="Please Enter Help Head">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <select name="status" required class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Upload Image</label>
                                <div class="col-lg-6">
                                    <input name="image"  ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-info'}" type="file">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Content</label>
                                <div class="col-lg-10">
                                    <textarea name="contents" value="{{ old('contents') }}" class="form-control"></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Create New Help</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/bootstrap3-wysiwyg/bootstrap3-wysihtml5.min.js') }}"></script>
    <script>
        $('textarea[name="contents"]').wysihtml5();
    </script>

@endsection
