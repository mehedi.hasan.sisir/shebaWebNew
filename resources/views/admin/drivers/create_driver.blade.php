@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Create Driver</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Create A New Driver </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('drivers.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input name="driver_name" required type="text" class="form-control" placeholder="Please Enter Driver Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Ride Type</label>
                                <div class="col-lg-8">
                                    <select name="ride_id" required class="form-control">
                                        <option value=""></option>
                                        @foreach($rideTypes as $ridetype)
                                            <option value="{{$ridetype->_id}}" >{{$ridetype->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Phone</label>
                                <div class="col-lg-8">
                                    <input name="phone" required type="text" class="form-control" placeholder="Please Enter Driver Phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Email</label>
                                <div class="col-lg-8">
                                    <input name="email" type="text" class="form-control" placeholder="Please Enter Driver Email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Password</label>
                                <div class="col-lg-8">
                                    <input name="password" type="password" required class="form-control" placeholder="Please Enter Driver Password">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Address</label>
                                <div class="col-lg-8">
                                    <input name="address" type="text" class="form-control" placeholder="Please Driver Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Post Code</label>
                                <div class="col-lg-8">
                                    <input name="post_code" type="text" class="form-control" placeholder="Please Enter Postcode">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Is Deaf</label>
                                <div class="col-lg-8">
                                    <select name="is_deaf" required class="form-control">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Image</label>
                                <div class="col-lg-8">
                                    <input name="image"  ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-info'}" type="file">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Reference Id</label>
                                <div class="col-lg-8">
                                    <input name="reference_id" type="text" class="form-control" placeholder="Please Enter Reference Id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Ride model</label>
                                <div class="col-lg-8">
                                    <input name="ride_model" type="text" class="form-control" placeholder="Please Enter Ride Model">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Registration No</label>
                                <div class="col-lg-8">
                                    <input name="vehicle_reg_no" type="text" class="form-control" placeholder="Please Enter Registration Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Licence Number</label>
                                <div class="col-lg-8">
                                    <input name="license_no" type="text" class="form-control" placeholder="Please Enter Licence Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">City</label>
                                <div class="col-lg-8">
                                    <input name="city" type="text" class="form-control" placeholder="Please Enter City">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">State</label>
                                <div class="col-lg-8">
                                    <input name="state" type="text" class="form-control" placeholder="Please Enter State">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Is Flash Needed</label>
                                <div class="col-lg-8">
                                    <select name="is_flash_required" required class="form-control">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Driver Type</label>
                                <div class="col-lg-8">
                                    <select name="driver_type" required class="form-control">
                                        <option value="0">Driver Also An Owner</option>
                                        <option value="1">Non Driving Partner</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Status</label>
                                <div class="col-lg-8">
                                    <select class="form-control">
                                        <option>By Default Inactive</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Create New Driver</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
