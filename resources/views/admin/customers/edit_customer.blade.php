@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Edit Customer</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Edit Data of {{$customer->name}} </h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('customers.update', $customer->_id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method('PUT')

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    <input name="name" required value="{{$customer->name}}" type="text" class="form-control" placeholder="Please Enter Customer Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Phone</label>
                                <div class="col-lg-8">
                                    <input name="phone" required value="{{$customer->phone}}" type="text" class="form-control" placeholder="Please Enter Customer Phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Email</label>
                                <div class="col-lg-8">
                                    <input name="email" required value="{{$customer->email}}" type="text" class="form-control" placeholder="Please Enter Customer Email">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">User Code</label>
                                <div class="col-lg-8">
                                    <input name="user_code" value="{{$customer->user_code}}" type="text" class="form-control" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-4 control-label">Image</label>
                                <div class="col-lg-8">
                                    <input name="image" ui-jq="filestyle" ui-options="{icon: false, buttonName: 'btn-primary'}" type="file">
                                    <hr>
                                    <img src="{{!empty($driver->image) ? asset('storage/customer/'.$driver->image) : ''}}" width="100px;" height="100px" />
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-lg-4 control-label">OTP</label>
                                <div class="col-lg-8">
                                    <input name="otp" value="{{$customer->otp}}" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Promo Code</label>
                                <div class="col-lg-8">
                                    <input name="promocode" value="{{$customer->promocode}}" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Affiliate Code</label>
                                <div class="col-lg-8">
                                    <input name="affiliate_code" value="{{$customer->affiliate_code}}" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">FCM Token</label>
                                <div class="col-lg-8">
                                    <textarea name="fcm_token" rows="6" class="form-control">{{$customer->fcm_token}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Status</label>
                                <div class="col-lg-8">
                                    <select name="status" required class="form-control">
                                        <option value=""></option>
                                        <option @if($customer->status == 0) selected='selected' @endif value="0">Inactive</option>
                                        <option @if($customer->status == 1) selected='selected' @endif value="1">Active</option>
                                        <option @if($customer->status == 2) selected='selected' @endif value="2">Deleted</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Update Customer</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection
