<div class="row">
    <div class="col-md-10 col-md-offset-1">
        @if ($errors->any())
            <div class="text-center alert" style="margin-bottom:0px !important;">
                <ul class="list-group">
                    @foreach ($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>