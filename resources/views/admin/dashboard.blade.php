@extends('layouts.app')

@section('content')

    <!-- main header -->
    <div class="bg-light lter b-b wrapper-md">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <h1 class="m-n font-thin h3 text-black">Dashboard</h1>
            </div>
            <div class="col-sm-6 text-right hidden-xs">

            </div>
        </div>
    </div>

    <div class="wrapper-md">
        <div class="row">
            <div class="col-md-12">
                <div class="row row-sm text-center">
                    <div class="col-xs-3">
                        <div class="panel padder-v bg-primary  item">
                            <div class="h1 font-thin h1">{{ $totalRides }}</div>
                            <span class="text-white text-xs">Running Rides</span>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="block panel padder-v bg-white item">
                            <span class="font-thin h1 block">{{ $totalCustomer }}</span>
                            <span class="text-muted text-xs">Total Customers</span>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="block panel padder-v bg-dark item">
                            <span class="text-white font-thin h1 block">{{ $totalDriver }}</span>
                            <span class="text-white text-xs">Total Drivers</span>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="panel padder-v  bg-info item">
                            <div class="font-thin h1">{{ $totalBooking }}</div>
                            <span class="text-white text-xs">Total Bookings</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

