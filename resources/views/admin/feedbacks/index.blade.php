@extends('layouts.app')

@section('content')

    <div class="row bg-light lter b-b wrapper-md">
        <div class="col-md-12">
            <h1 class="m-n font-thin h3">Dashboard / All Feedback</h1>
        </div>
    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Feedback List</h4>
            </div>
            <div class="table-responsive">
                <table id="feedback_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('feedbacks.json')}}',
              aoColumns: allFeedbackColumns, bProcessing: true}" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-1">Booking Id</th>
                        <th class="col-md-1">Customer</th>
                        <th class="col-md-1">Driver</th>
                        <th class="col-md-1">Rating</th>
                        <th class="col-md-3">Driver Feedback</th>
                        <th class="col-md-1">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="feebackDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Feedback Details</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading"><span id="driver_name"></span></div>
                        <table id="modal_feedback_details" class="table table-striped m-b-none">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var allFeedbackColumns = [

            {
                mData: 'booked_id',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },
            {
                mData: 'customer_name',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },
            {
                mData: 'driver_name',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },
            {
                mData: 'rating',
                render: function (data, type, row, meta) {
                    return '<i class="fa fa-star text-warning m-r-xs"></i>' + data;
                }
            },
            {
                mData: 'driver_feedback',
                render: function (data, type, row, meta) {
                    return (data) ? data : ' ';
                }
            },

            {
                mData: '_id',
                render: function (data, type, row, meta) {
                    return '<button id="feebackDetailsModal" type="button" class="btn btn-sm btn-primary"' +
                        'data-toggle="modal" data-target="#feebackDetails"' +
                        'data-id="' + data + '">Details</button>';
                }
            },
        ];

        $('table#feedback_table').on('click', 'button#feebackDetailsModal', function () {
            var id = $(this).data('id');

            $.get("{{url('feedbacks')}}/" + id, function (response) {

                var generatedTableHtml = '';
                $.each(response, function (key, value) {
                    var keySting = key.replace(/_/gi, ' ');
                    var UpperCaseKey = keySting[0].toUpperCase() + keySting.substr(1);

                    generatedTableHtml += '<tr><td>' + UpperCaseKey + '</td>' +
                        '<td>' + value + '</td></tr>';
                });

                $('#feebackDetails table#modal_feedback_details tbody').html(generatedTableHtml)

            }, "json");
        })
    </script>
@endsection