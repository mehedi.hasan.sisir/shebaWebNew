@extends('layouts.app')

@section('content')

    <div class="bg-light lter b-b wrapper-md row">
        <div class="col-md-10">
            <h1 class="m-n font-thin h3">Dashboard / All Notifications</h1>
        </div>

    </div>
    <div class="wrapper-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>All Notifications List</h4>
            </div>
            <div class="table-responsive">
                <table id="driver_table" ui-jq="dataTable" ui-options="{sAjaxSource: '{{route('notifications.json')}}',
              aoColumns: allNotificationColumns
        }" class="table table-striped b-t b-b">
                    <thead>
                    <tr>
                        <th class="col-md-2">Title</th>
                        <th class="col-md-3">Message</th>
                        <th class="col-md-1">type</th>
                        <th class="col-md-1">Image</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-1">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var allNotificationColumns = [
            {mData: 'title'},
            {mData: 'description'},
            {mData: 'type'},
            {
                mData: 'image',
                render: function (data, type, row, meta) {
                    if(data){

                        return '<img src="{{asset('storage/notifications/')}}/' + data + '" class="img-responsive" style="width: auto; height: 70px;">';
                    }else{
                        return '<img src="{{asset('storage/images/no_image.jpg')}}" class="img-responsive" style="width: auto; height: 70px;">';
                    }
                }
            },
            {
                mData: 'status',
                render: function (data, type, row, meta) {
                    if(data == 1){
                        return '<span class="label label-success">Success</span>';
                    }else{
                        return '<span class="label label-danger">Failed</span>';
                    }
                }
            },
            {mData: 'created_at'},
        ];
    </script>
@endsection