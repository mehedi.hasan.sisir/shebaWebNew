@extends('layouts.app')


@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Dashboard / Update FCM Text</h1>
    </div>
    @include('admin.show_error_message')
    <div class="wrapper-md">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    <h4>Existing FCM Key</h4>
                </div>
                <div class="panel-body">
                    <form class="bs-example form-horizontal" action="{{route('notification.fcm.update',$fcmKey->_id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">FCM URL</label>
                                <div class="col-lg-10">
                                    <input name="fcm_url"  value="{{$fcmKey->fcm_url}}" required type="text" class="form-control" placeholder="Please Enter FCM Url">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">FCM Key</label>
                                <div class="col-lg-10">
                                    <input name="fcm_key_id"  value="{{$fcmKey->fcm_key_id}}" required type="text" class="form-control" placeholder="Please Enter FCM Key">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <hr>
                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update FCM Info</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('libs/jquery/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $('input[name="start_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
        $('input[name="expiration_date"]').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            todayHighlight: true
        });
    </script>

@endsection