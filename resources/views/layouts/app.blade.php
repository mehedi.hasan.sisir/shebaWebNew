<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('assets/img/favicon.png') }}" type="image/gif" sizes="16x16">

    <title>{{(\App\Setting::getHeaderText()) ? \App\Setting::getHeaderText() : 'Sheba'}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('libs/assets/animate.css/animate.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('assets/css/font.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}" type="text/css"/>
    @yield('style')

</head>

<body>


<div class="app app-header-fixed ">

    @include('layouts.header')


    @include('layouts.sidebar')




    <div id="content" class="app-content" role="main">

        <div class="app-content-body ">

            <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="app.settings.asideFolded = false; app.settings.asideDock = false;">

                @include('admin.show_flash_message')

                @yield('content')

            </div>
        </div>
    </div>

    <!-- footer -->
    <footer id="footer" class="app-footer" role="footer">
        <div class="wrapper b-t bg-light">
            <span class="pull-right">v. 1.01 <a href ui-scroll="app" class="m-l-sm text-muted"><i
                            class="fa fa-long-arrow-up"></i></a></span>
            {{(\App\Setting::getFooterText()) ? \App\Setting::getFooterText() : '&copy; Sheba Transcome'}}
        </div>
    </footer>
</div>


<script src="{{ asset('libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ asset('libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/ui-load.js') }}"></script>
<script src="{{ asset('assets/js/ui-jp.config.js') }}"></script>
<script src="{{ asset('assets/js/ui-jp.js') }}"></script>
<script src="{{ asset('assets/js/ui-nav.js') }}"></script>
<script src="{{ asset('assets/js/ui-toggle.js') }}"></script>
<script src="{{ asset('assets/js/ui-client.js') }}"></script>
@yield('script')
</body>
</html>
