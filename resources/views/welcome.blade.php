<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sheba-bd</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="{{asset('assets/css/style.min.css')}}" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>


    <div class="container">
        <div class="row">
            <hr>
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="well well-sm text-center">
                            <div class="icon" style="font-size:100px;">
                                <i class="glyphicon glyphicon-road"></i>
                            </div>
                            <div class="options">
                                @if (Route::has('login'))
                                    <div class="top-right links">
                                        @auth
                                            <a class="btn btn-primary btn-lg" href="{{ url('/dashboard') }}">Dashboard</a>
                                        @else
                                            <a class="btn btn-primary btn-lg" href="{{ route('login') }}">Login</a>
                                        @endauth
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Sheba
                </div>
            </div>
        </div>
    </body>
</html>
