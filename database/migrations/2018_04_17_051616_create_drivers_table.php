<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver_name');
            $table->string('name');
            $table->integer('car_id');
            $table->string('image', 750);
            $table->string('post_code');
            $table->string('vehicle_reg_no');
            $table->string('phone', 25);
            $table->string('email', 50);
            $table->string('city', 100);
            $table->string('state', 50);
            $table->string('password', 50);
            $table->text('address', 50);
            $table->text('driver_lat');
            $table->text('driver_lng');
            $table->text('license_no', 25);
            $table->tinyInteger('status')->default(1)->comment('1=>Active,0=>Inactive,2=>delete');
            $table->tinyInteger('book_status')->default(1)->comment('1- booking, 2- Processing, 0 - Free/Completed');
            $table->tinyInteger('is_online')->comment('1- Online, 0- Offline');
            $table->text('fcm_token');
            $table->tinyInteger('driver_type')->comment('0=>Driver come Owner,1=>Non-Driving Partner');
            $table->tinyInteger('is_deaf')->comment('true=>1,false=>0');
            $table->tinyInteger('is_flash_required')->comment('true=>1,false=>0');
            $table->string('affiliate_code');
            $table->string('reference_id');
            $table->timestamp('create_date')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
