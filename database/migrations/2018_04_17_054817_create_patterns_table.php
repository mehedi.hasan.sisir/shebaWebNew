<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patterns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('car_name', 100);
            $table->integer('car_type');
            $table->string('location', 100);
            $table->string('range', 100);
            $table->float('base_price');
            $table->float('min_rid_fare');
            $table->float('km_rate');
            $table->float('min_rate');
            $table->string('lat');
            $table->string('lng');
            $table->string('currency', 10)->collation('utf8_unicode_ci');
            $table->tinyInteger('status')->default(1)->comment('1=>Active,0=>Inactive,2=>delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patterns');
    }
}
