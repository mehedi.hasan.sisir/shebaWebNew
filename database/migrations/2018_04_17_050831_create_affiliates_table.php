<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('user_reference_id',100);
            $table->enum('user_type', ['Customer', 'Driver']);
            $table->float('commission');
            $table->enum('commission_type', ['percentage', 'add']);
            $table->string('commis_limit');
            $table->enum('commis_limit_type', ['one', 'day','monthly','yearly']);
            $table->timestamp('start_time')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliates');
    }
}
