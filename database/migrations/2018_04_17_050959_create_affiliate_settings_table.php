<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commission');
            $table->enum('commission_type', ['percentage', 'add']);
            $table->enum('commission_user_type', ['Customer', 'Driver']);
            $table->float('commission_limit');
            $table->enum('commission_limit_type', ['one','day','monthly','yearly']);
            $table->integer('end_time');
            $table->enum('end_time_type', ['one','day','monthly','yearly']);
            $table->tinyInteger('status')->default(1)->comment('1=>Active,0=>Inactive,2=>delete');
            $table->timestamp('create_date')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_settings');
    }
}
