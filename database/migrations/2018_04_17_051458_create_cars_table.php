<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id');
            $table->integer('type');
            $table->string('model', 50);
            $table->string('car_owner', 100);
            $table->string('vehicle_reg_num', 100);
            $table->tinyInteger('max_seat');
            $table->double('min_fare');
            $table->double('km_fare');
            $table->double('time_fare');
            $table->string('image');
            $table->tinyInteger('status')->default(1)->comment('1=>Active, 0=>Inactive, 2=>Delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
