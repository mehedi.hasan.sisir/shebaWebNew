<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('admins')->insert([
            ['name' => "Admin",
                'user_name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('123456'),
                'role_id' => 1,
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            ['name' => "Admin",
                'user_name' => 'admin',
                'email' => 'firoj.mahmud@gmail.com',
                'password' => bcrypt('admin'),
                'role_id' => 1,
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ]
        ]);

        DB::table('roles')->insert([
            [
                'name' => 'super',
                'description' => 'Super Admin User',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            [
                'name' => 'admin',
                'description' => 'Admin User',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            [
                'name' => 'moderator',
                'description' => 'Moderator',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ]
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'read',
                'description' => 'Read',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            [
                'name' => 'write',
                'description' => 'Write',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            [
                'name' => 'update',
                'description' => 'Update',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ],
            [
                'name' => 'delete',
                'description' => 'Delete',
                'created_at' => '2018-04-16 00:00:00.000000',
                'updated_at' => '2018-04-16 00:00:00.000000',
            ]
        ]);


        DB::table('settings')->insert([
            [
                'title' => 'SHEBA',
                'logo' => 'assets/uploads/settings//1516862721.png',
                'favicon' => '',
                'tax' => '0',
                'admin_charge' => '20',
                'booking_code' => 'SHB',
                'browser_api_key' => 'AIzaSyDZWL3BZwZLAwWcng7nGYKCBvyqMuV4mPA',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
        ]);


        DB::table('promocodes')->insert([
            [
                'code' => '75BON',
                'start_date' => '2018-04-03',
                'expiration_date' => '2018-04-05',
                'type' => '2',
                'off' => '100',
                'promo_type' => '0',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
        ]);


        DB::table('help_tables')->insert([
            [
                'image' => 'assets/uploads/testimonials/ic_help_payment_rewards1.png',
                'head' => 'Payment  Rewards',
                'content' => '<p><span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
            [
                'image' => 'assets/uploads/testimonials/ic_help_sign_up_to_drive.png',
                'head' => 'Signup to Drive',
                'content' => '<p><span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
            [
                'image' => 'assets/uploads/testimonials/ic_help_account.png',
                'head' => 'Account',
                'content' => '<p><span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
            [
                'image' => 'assets/uploads/testimonials/ic_help_5_star_program_guide.png',
                'head' => '5 Star Partner Guide',
                'content' => '<p><span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
            [
                'image' => 'assets/uploads/testimonials/ic_help_safety_security.png',
                'head' => 'Safety  Security',
                'content' => '<p><span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>',
                'status' => '1',
                'created_at' => '2018-04-17 00:00:00.000000',
                'updated_at' => '2018-04-17 00:00:00.000000',
            ],
        ]);


        DB::table('permission_role')->insert([
            [
                'permission_id' => 1,
                'role_id' => 1
            ],
            [
                'permission_id' => 2,
                'role_id' => 1
            ],
            [
                'permission_id' => 3,
                'role_id' => 1
            ],
            [
                'permission_id' => 4,
                'role_id' => 1
            ],
            [
                'permission_id' => 1,
                'role_id' => 2
            ],
            [
                'permission_id' => 2,
                'role_id' => 2
            ],
            [
                'permission_id' => 1,
                'role_id' => 3
            ],
        ]);
    }
}
