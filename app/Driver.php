<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Driver extends \Jenssegers\Mongodb\Eloquent\Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract{

    use Authenticatable, Authorizable, CanResetPassword, HasApiTokens,SoftDeletes;

    protected $dates = ['deleted_at'];

    public function rideType()
    {
        return $this->belongsTo('App\RideTypes', 'ride_id');
    }

    public function balance()
    {
        return $this->hasOne('App\Balance', 'user_id');
    }


    public function token()
    {
        return $this->hasOne(\Laravel\Passport\Token::class, 'user_id');
    }


    public function documents()
    {
        return $this->hasMany(\App\DriverDocument::class);
    }



    public static function getDriverRating($driverId){
        // AVG(rating) AS rating')->where('rating >',0)->where('driver_id',$id)->get('feedback')

        $driverRating = DB::table('feedback')
                        ->where('rating','>',0)
                        ->where('driver_id', $driverId)
                        ->avg('rating');

        return intval($driverRating);
    }

    public static function getDriverType($type){

        return ($type ==  1) ? 'Driver Also Owner' : 'Non - Driving Partner';
    }


}
