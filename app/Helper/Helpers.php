<?php
/**
 * Created by Mehedi.
 * Date: 4/24/2018
 * Time: 11:45 AM
 */

namespace App\Helper;

use App\Util\Constant;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class Helpers
{
    public static function getDriverAffiliateCount($affiliateId){

        return DB::collection('driver')->where('reference_id', $affiliateId)->count();
    }


    public static function getCustomerAffiliateCount($affiliateId){

        return DB::collection('customer')->where('reference_id', $affiliateId)->count();
    }

    public static function getStatusName($status){

        if($status == Constant::ACTIVE){
            return 'Active';
        }else if($status == Constant::INACTIVE){
            return 'Inactive';
        }else if($status == Constant::DELETED){
            return 'Deleted';
        }else{
            return '';
        }
    }

    public static function getCssColor($status){

        $cssArray = [
            '0'=>'danger',
            '1'=>'success',
            '2'=>'warning',
            '3'=>'info',
        ];

        return $cssArray[$status];
    }

    public static function getYesOrNo($status){

        return ($status ==  1) ? 'Yes' : 'No';
    }



    public static function returnSuccessData($data, $message = ''){

        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => $message

        ],Response::HTTP_ACCEPTED);
    }

    public static function returnErrorMsg($errorCode, $message = ''){

        return response()->json([
            'success' => false,
            'data' => null,
            'message' => $message

        ],Response::HTTP_NOT_FOUND);
    }
}