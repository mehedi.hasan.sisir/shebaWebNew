<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class RideTypes extends Eloquent
{
    protected $collection = 'ride_types';

    use SoftDeletes;
}
