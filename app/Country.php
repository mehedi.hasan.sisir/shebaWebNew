<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Country extends Eloquent
{
    protected $collection = 'countries';

    public static function getCountryByName($countryName = ''){

        if(!empty($countryName)){

            return Country::where('name', $countryName)->first();
        }else{
            return "";
        }

    }
}
