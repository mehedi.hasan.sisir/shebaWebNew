<?php

namespace App\Http\Resources\BookingResource;

use App\Booking;
use App\Helper\Helpers;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingsResourceForDatatable extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'id' => $this->id,
            'booking_id' => $this->booking_id,
            'source' => $this->source,
            'destination' => $this->destination,
            'customer_name' => $this->customer_name,
            'driver_name' => $this->driver_name,
            'car_name' => Booking::getCarNameFromCarType($this->ride_type),
            'book_date' => date('d-M-Y', $this->book_date),
            'fare' => $this->fare,
            'status' => Booking::getBookingStatus($this->status),
            'label_colors' => Booking::getCssColor($this->status),
        ];
    }
}
