<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Resources\FeedbackResource\FeedbackSingleResource;

class FeedbackController extends Controller
{

    public function index(){

        return view('admin.feedbacks.index');
    }

    public function getFeedbackJson(){

        return response()->json([

            'aaData' => Feedback::orderBy('_id', 'desc')->get()

        ]);
    }


    public function show(Feedback $feedback){

        return response()->json(new FeedbackSingleResource($feedback));
    }
}
