<?php

namespace App\Http\Controllers;

use App\HelpReview;
use App\HelpTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class HelpTableController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        $helpTable = HelpTable::all();

        return view('admin.helps.index')->with('helpTable', $helpTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.helps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $help = new HelpTable();
        $help->head = $request->head;
        $help->content = $request->contents;
        $help->status = intval($request->status);
        if ($request->hasFile('image')) {

            $help->image = $this->uploadImage($request->file('image'));

        } else {

            $help->image = "";
        }
        $help->save();
        return redirect()->route('helps.create')->with('success', 'Help Text Successfully Created');
    }

    public function show(HelpTable $help)
    {
        return response()->json([
            'head' => $help->head,
            'content' => $help->content,
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(HelpTable $help)
    {
        return view('admin.helps.edit')->with('help', $help);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, HelpTable $help)
    {

        $this->validator($request->all())->validate();

        $help->head = $request->head;
        $help->content = $request->contents;
        $help->status = intval($request->status);

        if ($request->hasFile('image')) {

            $help->image = $this->uploadImage($request->file('image'));
        }
        $help->save();

        return redirect()->route('helps.edit',$help->_id)->with('success', 'Help Text Successfully Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(HelpTable $help)
    {
        $help->delete();
        return redirect()->route('helps.index')->with('success', ' Help Text Successfully Deleted');
    }


    /**
     * Validate the request.
     *
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
                'head' => 'required',
                'contents' => 'required',
                'image' => 'image|max:1000',
            ]);
    }

    /**
     * upload image to storage/app/public/ride
     */
    public function uploadImage($image){

        $imageName = uniqid() . time() . '.' . $image->getClientOriginalExtension();

        Image::make($image)->resize(300, null, function ($constraint) {

            $constraint->aspectRatio();

        })->save(storage_path('app/public/images/' . $imageName));

        return $imageName;
    }
}
