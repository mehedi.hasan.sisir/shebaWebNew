<?php

namespace App\Http\Controllers;

use App\Driver;
use App\DriverDocument;
use App\Helper\Helpers;
use App\Http\Resources\DriverResource\DriverDocumentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\Console\Helper\Helper;

class DriverDocumentController extends Controller
{

    /**
     * Display a listing of this Resources.
     *
     */
    public function index()
    {

        return view('admin.documents.index');
    }

    /**
     * Show the get Document in json format
     *
     */
    public function getDocumentJson()
    {
        $driverWithDocuments =  Driver::raw(function($collection)
        {
            return $collection->aggregate(
                [[
                    '$lookup' => [
                        'from'=>'driver_documents',
                        'localField'=>'_id',
                        'foreignField'=>'driver_reference_id',
                        'as'=>'driver_documents'
                    ]
                ]]
            );
        });

        return response()->json([

            'aaData' => DriverDocumentResource::collection($driverWithDocuments)

        ]);
    }
    /**
     * Show the form for creating a new Resources.
     *
     */


    public function create()
    {

    }

    /**
     * Store a newly created Resources in storage.
     *
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Store a newly created Resources in storage.
     *
     */
    public function getAllDocumentsOfDriver(Driver $driver)
    {

        $documents = $driver->documents;
        $cssArray = $this->colorArrayByIndex();

        return view('admin.documents.modal_documents_of_driver')->with('documents', $documents)->with('allCssColor',$cssArray);
    }

    /**
     * Display the specified Resources.
     *
     */
    public function show(DriverDocument $document){

        $document['type_name'] = DriverDocument::documentTypeName($document->type);
        $document['status_css'] = $this->getCssColorByType($document->status);

        return view('admin.documents.modal_single_documents_of_driver')->with('document', $document);


    }


    /**
     * Update the specified Resources in Database.
     *
     */
    public function update(Request $request, DriverDocument $document)
    {
        $document->status = $request->status;
        $document->save();

        $cssColor = $this->getCssColorByType($document->status);


        return Response::json(array('message' => 'Document Updated', 'css_color' => $cssColor));
    }

    /**
     * use Soft delete to the specified Resources from Database.
     *
     */
    public function destroy($id)
    {
        //
    }

    public function colorArrayByIndex(){

        return DriverDocument::getAllCssColorByDocumentStatus();

    }

    public function getAllStatus(){

        return DriverDocument::getAllStatusAsTextOfDocuments();
    }

    public function getCssColorByType($status)
    {

        return $this->colorArrayByIndex()[$status];
    }
    public function getStatusName($status)
    {

        return $this->getAllStatus()[$status];
    }
}
