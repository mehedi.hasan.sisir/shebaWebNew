<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Resources\DriverResource\DriverResourceForDatatable;
use App\Http\Resources\DriverResource\DriverSingleResource;
use App\RideTypes;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class DriverController extends Controller
{
    /**
     * Display all Driver
     */
    public function index()
    {

        return view('admin.drivers.all_drivers');

    }
    /**
     * create a new Driver
     */
    public function create()
    {

        $rideTypes = RideTypes::all();

        return view('admin.drivers.create_driver')->with('rideTypes', $rideTypes);

    }

    /**
     * store a new driver
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $driver = new Driver();
        $driver->driver_name = $request->driver_name;
        $driver->ride_id = $request->ride_id;
        $driver->ride_model = $request->ride_model;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->password = md5($request->password);
        $driver->address = $request->address;
        $driver->post_code = $request->post_code;
        $driver->city = $request->city;
        $driver->state = $request->state;
        $driver->is_deaf = intval($request->is_deaf);
        $driver->is_flash_required = intval($request->is_flash_required);
        $driver->vehicle_reg_no = $request->vehicle_reg_no;
        $driver->license_no = $request->license_no;
        $driver->driver_type = intval($request->driver_type);
        $driver->reference_id = $request->reference_id;
        $driver->status = 0;
        $driver->book_status = 0;
        $driver->is_online = 0;
        $driver->fcm_token = "";
        $driver->affiliate_code = 'DA'.str_random(6);
        $driver->location = [];
        $driver->deleted_at = null;


        if ($request->hasFile('image')) {

            $driver->image = $this->uploadImage($request->file('image'));
        }else{
            $driver->image = '';
        }

        $driver->save();

        return redirect()->route('drivers.create')->with('success', 'Driver Successfully Created');
    }

    /**
     * Display all Driver in json
     */
    public function allDriverJson()
    {

        return response()->json([

            'aaData' => DriverResourceForDatatable::collection(Driver::all())

        ]);

    }

    /**
     * Display single Driver in json
     */
    public function singleDriverJson(Driver $driver)
    {

        return response()->json(new DriverSingleResource($driver));

    }


    public function edit(Driver $driver)
    {
        $rideTypes = RideTypes::all();

        return view('admin.drivers.edit_driver', compact(['driver', 'rideTypes']));
    }


    public function update(Request $request, Driver $driver)
    {
        $this->validator($request->all())->validate();

        $driver->driver_name = $request->driver_name;
        $driver->ride_id = $request->ride_id;
        $driver->ride_model = $request->ride_model;
        $driver->phone = $request->phone;
        $driver->email = $request->email;
        $driver->address = $request->address;
        $driver->post_code = $request->post_code;
        $driver->city = $request->city;
        $driver->state = $request->state;
        $driver->is_deaf = intval($request->is_deaf);
        $driver->is_flash_required = intval($request->is_flash_required);
        $driver->vehicle_reg_no = $request->vehicle_reg_no;
        $driver->license_no = $request->license_no;
        $driver->driver_type = intval($request->driver_type);
        $driver->status = intval($request->status);

        if ($request->hasFile('image')) {
            $imagePath = storage_path('app/public/driver/'. $driver->image);

            if (File::exists($imagePath)) {
                File::delete($imagePath);
            }

            $driver->image = $this->uploadImage($request->file('image'));;
        }

        if($driver->save()){

            return redirect('/drivers/'.$driver->_id.'/edit')->with('success', 'Driver Successfully Updated');
        }else{

            return redirect('/drivers/'.$driver->_id.'/edit')->with('error', 'Sorry ! Update Failed');
        }

    }

    public function delete(Driver $driver)
    {
        $driver->delete();
        return redirect()->route('all.drivers')->with('success', $driver->name.' Data Successfully Deleted');

    }

    /**
     * Validate the request.
     *
     */
    public function validator(array $requestData)
    {
        return Validator::make($requestData,
            [
                'driver_name' => 'required',
                'phone' => 'required|unique:users,phone,'.$requestData['phone'],
                'ride_id' => 'required',
                'status' => 'required',
                'image' => 'image|max:1000',
            ]);
    }

    /**
     * upload image to storage/app/public/ride
     */
    public function uploadImage($image){

        $imageName = uniqid() . time() . '.' . $image->getClientOriginalExtension();

        if(!File::exists(storage_path('app/public/driver'))) {

            File::makeDirectory(storage_path('app/public/driver'), $mode = 0777, true, true);
        }

        Image::make($image)->resize(300, null, function ($constraint) {

            $constraint->aspectRatio();

        })->save(storage_path('app/public/driver/' . $imageName));

        return $imageName;
    }


}
