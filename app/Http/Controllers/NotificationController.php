<?php

namespace App\Http\Controllers;

use App\FcmKey;
use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    /**
     * Show all FCM from fcmkey database.
     *
     */
    public function viewFcmPage(){

        $fcmKey = FcmKey::first();

        return view('admin.notifications.fcm_edit')->with('fcmKey', $fcmKey);
    }

    /**
     * edit single fcm
     *
     */
    public function editFcm(Request $request, FcmKey $fcm){

        $fcm->fcm_url = $request->fcm_url ;
        $fcm->fcm_key_id = $request->fcm_key_id ;
        $fcm->save();

        return redirect()->route('notification.fcm')->with('success', 'FCM Info Successfully Deleted');

    }

    /**
     * Display a listing of this Notifications.
     *
     */
    public function index()
    {
        return view('admin.notifications.index');
    }

    /**
     * Display a listing of this Notifications.
     *
     */
    public function getNotificationJson()
    {

        return response()->json([
            'aaData' => Notification::all()
        ]);
    }

    /**
     * Show the form for creating a new Notifications.
     *
     */
    public function create()
    {
        return view('admin.notifications.create');
    }

    /**
     * Store a newly created Notifications in storage.
     *
     */
    public function store(Request $request)
    {
        //
    }


}
