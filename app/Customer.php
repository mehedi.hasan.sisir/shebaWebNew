<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;


use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Customer extends \Jenssegers\Mongodb\Eloquent\Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract{

    use Authenticatable, Authorizable, CanResetPassword, HasApiTokens,SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function getNumberOfBooking($customerId){

        return DB::collection('bookings')->where('customer_id', $customerId)->count();
    }

    public static function refererCustomer($customerId){
        $customer =  Customer::find($customerId);

        return isset($customer->name) ? $customer->name : '';
    }


}
