<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Request extends Eloquent
{

    public function ride(){

        return $this->belongsTo(\App\Booking::class, 'trip_id');
    }

    public function customer(){

        return $this->belongsTo(\App\Customer::class);
    }

    public function driver(){

        return $this->belongsTo(\App\Driver::class);
    }


    public static function allRequestStatus(){
        return [
                '1'=>'Assigned',

                '2'=>'Failed',

                '3'=>'Cancelled',

                '0'=>'Pending'
            ];
    }


    public static function getRequestStatus($status){


        return Request::allRequestStatus()[$status];

    }
}
