<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Pattern extends Eloquent
{
    use SoftDeletes;


    public function rideType(){

        return $this->belongsTo('App\RideTypes', 'ride_type_id');
    }
}
