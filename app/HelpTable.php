<?php

namespace App;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class HelpTable extends Eloquent
{

    public function helpReview(){

        return $this->hasMany('App\HelpReview', 'help_id');
    }


    public function likes(){

        return $this->helpReview->where('help_status', 1)->count();

    }
    public function dislike(){

        return $this->helpReview->where('help_status', 0)->count();

    }

}
