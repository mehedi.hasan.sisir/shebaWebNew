<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = ['name', 'permissions', ];
//
//    public function users()
//    {
//        return $this->hasOne(User::class);
//    }


    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }

    public function hasPermission($userPermissions)
    {
        foreach ($this->permissions as $permission) {

            if($permission->name == $userPermissions){
                return true;
            }
        }
        return false;
    }

//    private function hasPermission(string $permission)
//    {
//        return $this->permissions[$permission] ?? false;
//    }
}
