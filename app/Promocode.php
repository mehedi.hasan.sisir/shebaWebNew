<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Promocode extends Eloquent
{
    protected $dates = ['start_date', 'expiration_date'];
}
