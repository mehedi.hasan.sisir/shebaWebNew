Sheba Web Api List
_______________________________________________________

1.	shebaweb.test/api/v1/driver/register
    {
        "name" : "Mehedi Driver",
        "email" : "mehedi@gmail.com",
        "phone" : "+8801675601998",
        "password" : "123456",
        "c_password" : "123456",
        "post_code" : "1215",
        "city" : "Tangail",
        "state" : "Gopalpur",
        "is_deaf" : 0,
        "is_flash_required" : 1,
        "vehicle_reg_no" : "DHK23124",
        "license_no" : "320840329470274",
        "driver_type" : 1,
        "reference_id" : "No ref"
    }

2.	shebaweb.test/api/v1/driver/login
    {
        "email": "+8801777333677",
        "password": "123456"
    }

3.	shebaweb.test/api/v1/driver/logout
    ## **  Headers ** ##
    [
    "Accept" : "application/json",
    "Authorization" : "Bearer yYWFhZm………..",
    ]

4.	shebaweb.test/api/v1/customer/register
    {
        "name" : "Mehedi Customer",
        "email" : "mehedi@customer.com",
        "phone" : "+8801675601998",
        "password" : "123456",
        "c_password" : "123456",
        "fcm_token" : "dENFon……….",
        "reference_id" : "5adb1195bfd35ffff",
        "promocode" : ""
    }

5.	shebaweb.test/api/v1/customer/login
    {
        "email": "+8801777333677",
        "password": "123456"
    }

6.	shebaweb.test/api/v1/customer/logout
    ## **  Headers ** ##
    [
    "Accept" : "application/json",
    "Authorization" : "Bearer WzAyyKp………..",
    ]
