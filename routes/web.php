<?php


Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

// Authentication Routes...



Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// switched off Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::middleware(['auth'])->group(function () {

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/home', 'DashboardController@index')->name('dashboard');

    Route::get('/bookings', 'BookingController@index')->name('all.bookings');
    Route::get('/bookings-json', 'BookingController@allBookingsJson')->name('all.bookings.json');

    Route::get('/bookings/{parameter}', 'BookingController@showSplitBooking')->name('split.bookings');
    Route::get('/bookings-json/{parameter}', 'BookingController@returnSplitBookingJson')->name('split.bookings.json');
    /*
     * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     */
    Route::get('/customers', 'CustomerController@index')->name('all.customers');
    Route::get('/customers-json', 'CustomerController@allCustomersJson')->name('all.customers.json');
    Route::get('/customers-json/{id}', 'CustomerController@singleCustomersJson');
    Route::get('/customers/{customer}/edit', 'CustomerController@edit');
    Route::put('/customers/{customer}', 'CustomerController@update')->name('customers.update');;
    Route::delete('/customers/{customer}', 'CustomerController@delete');

    /*
     * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     */
    Route::get('/drivers', 'DriverController@index')->name('all.drivers');
    Route::get('/drivers-json', 'DriverController@allDriverJson')->name('all.drivers.json');
    Route::get('/drivers-json/{driver}', 'DriverController@singleDriverJson');
    Route::get('/drivers/{driver}/edit', 'DriverController@edit');
    Route::get('/drivers/create', 'DriverController@create')->name('drivers.create');
    Route::post('/drivers', 'DriverController@store')->name('drivers.store');
    Route::put('/drivers/{driver}', 'DriverController@update')->name('all.driver.update');;
    Route::delete('/drivers/{driver}', 'DriverController@delete');
    /*
     * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     */
    Route::get('/drivers/{driver}/payment', 'TransactionController@showPaymentPage')->name('driver.payment.create');
    Route::post('/drivers/{driver}/payment', 'TransactionController@paymentCalculate')->name('driver.payment.store');
    Route::get('/transaction', 'TransactionController@index')->name('all.transaction');
    Route::get('/transaction-json', 'TransactionController@allTransactionJson')->name('all.transaction.json');
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::resource('rides', 'RideTypesController');
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::get('currency-by-country/{countryName}', 'PatternController@getCurrencyByCountryName');
    Route::resource('patterns', 'PatternController')->except(['show']);
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::resource('helps', 'HelpTableController');
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::resource('promocodes', 'PromocodeController')->except(['show', 'destroy']);
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::get('documents-json', 'DriverDocumentController@getDocumentJson')->name('driver-documents.json');
    Route::get('driver-documents/{driver}', 'DriverDocumentController@getAllDocumentsOfDriver');
    Route::resource('documents', 'DriverDocumentController')->except(['edit']);
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::get('feedbacks', 'FeedbackController@index')->name('feedbacks.index');
    Route::get('feedbacks-json', 'FeedbackController@getFeedbackJson')->name('feedbacks.json');
    Route::get('feedbacks/{feedback}', 'FeedbackController@show');
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::get('requests', 'RequestController@index')->name('requests.index');
    Route::get('requests/{request}', 'RequestController@show');
    Route::get('requests-json', 'RequestController@getRequestsJson')->name('requests.json');
    /*
    * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    */
    Route::get('fcms', 'NotificationController@viewFcmPage')->name('notifications.fcm');
    Route::put('fcms/{fcm}/edit', 'NotificationController@editFcm')->name('notification.fcm.update');

    Route::get('notifications-json', 'NotificationController@getNotificationJson')->name('notifications.json');

    Route::get('notifications', 'NotificationController@index')->name('notifications.index');
    Route::get('notifications/create', 'NotificationController@create')->name('notifications.create');
    Route::post('notifications', 'NotificationController@store')->name('notifications.store');
    /*
     * =-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
     */
    Route::get('settings', 'SettingController@edit')->name('settings.edit');
    Route::put('settings/{setting}/edit', 'SettingController@update')->name('settings.update');

});

Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');